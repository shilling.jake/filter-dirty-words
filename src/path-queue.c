#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <path-queue.h>

/**
 * Adds a new path to the queue.
 *
 * @param head     The current head of the queue
 * @patam path     The path to push
 * @return         The new head of the queue
 */
struct path_queue *path_queue_push(struct path_queue *head, const char *path) {
  /* Allocate new queue node */
  struct path_queue *new_node =
      (struct path_queue *)malloc(sizeof(struct path_queue));
  if (!new_node) {
    /* Out of memeory, lets just crash */
    perror("Could not add a new path");
    exit(EXIT_FAILURE);
  }

  /* Initialize new node */
  new_node->path = path;

  /* If head is not null, add new node to the end of the queue.
     Otherwise new node is the first element */
  if (head) {
    if (head->prev) {
      head->prev->next = new_node;
      new_node->prev = head->prev;
      head->prev = new_node;
      new_node->next = head;
    } else {
      head->prev = new_node;
      head->next = new_node;
      new_node->next = head;
      new_node->prev = head;
    }

    return head;
  } else {
    new_node->next = NULL;
    new_node->prev = NULL;

    return new_node;
  }
}

/**
 * Get the path at the top of the queue.
 *
 * @param head            The head of this queue
 * @param return_address  pointer to where we can return the path
 * @return                the new head of the queue
 */
struct path_queue *path_queue_pop(struct path_queue *head,
                                  const char **return_address) {
  /* Do nothing if there is no return_address */
  if (!return_address) {
    return head;
  }

  /* Check if queue has a next element */
  if (head) {
    /* Set return_address */
    (*return_address) = head->path;

    /* Configure new head */
    struct path_queue *new_head = head->next;
    if (new_head) {
      if (head->prev == new_head) {
        new_head->next = NULL;
        new_head->prev = NULL;
      } else {
        new_head->prev = head->prev;
      }
    }

    free(head);
    return new_head;
  } else {
    /* Set return_address to NULL */
    (*return_address) = NULL;
    return NULL;
  }
}

/**
 * Free all memory used by this queue. The paths themselves
 * will not be free'd.
 *
 * @param head       Object to free
 */
void path_queue_destroy(struct path_queue *head) {
  /* Do nothing if head is null */
  if (head) {
    /* free children */
    struct path_queue *cur = head->next;
    while (cur && cur != head) {
      struct path_queue *tmp = cur->next;
      free(cur);
      cur = tmp;
    }

    free(head);
  }
}

/**
 * Check if a path is contained in this path queue.
 *
 * @params queue        The queue
 * @params path         The path
 * @return              1 if true, 0 if false
 */
int path_queue_contains(struct path_queue *head, const char *path) {
  /* Check queue empty */
  if (head) {
    /* Check if first item */
    if (!strcmp(head->path, path)) {
      return 1;
    }

    /* Check if later item */
    for (struct path_queue *cur = head->next; cur && cur != head;
         cur = cur->next) {
      if (!strcmp(cur->path, path))
        return 1;
    }
  }

  return 0;
}
