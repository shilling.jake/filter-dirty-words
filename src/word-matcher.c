#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <pthread.h>
#include <regex.h>

#include <path-queue.h>
#include <word-matcher.h>

static inline struct error_list *add_error(struct word_matcher *matcher,
                                           struct error_list *last,
                                           const char *path, char *line,
                                           int err_code) {
  struct error_list *node =
      (struct error_list *)malloc(sizeof(struct error_list *));
  if (!node) {
    return last;
  }

  node->path = path;
  node->line = line;
  node->err_code = err_code;
  node->next = NULL;

  if (last)
    last->next = node;
  else
    matcher->errors = node;

  return node;
}

static inline int read_next_regex(struct word_matcher *matcher, FILE *file,
                                  char **__line, int *err_code) {
  (*__line) = NULL;
  char *line = NULL;
  size_t line_size = 0;

  do {
    /* Buffer might already have stuff in it, or we might need to read some
       more. */
    while (matcher->index < matcher->read) {
      size_t start = matcher->index;
      while ((matcher->index < matcher->read) &&
             ('\n' != matcher->buffer[matcher->index])) {
        matcher->index++;
      }

      size_t chunk_size = matcher->index - start;
      if (0 == chunk_size) {
        matcher->index++;
        continue;
      }

      line = realloc(line, line_size + chunk_size + 1);
      if (!line) {
        (*__line) = NULL;
        (*err_code) = errno;
        return -1;
      }

      memcpy(line + line_size, matcher->buffer + start, chunk_size);
      line_size += chunk_size;
      line[line_size] = '\0';

      (*__line) = line;

      /* Check if we've found a line or just need to read
         more. */
      if ('\n' == matcher->buffer[matcher->index]) {
        matcher->index++;
        return 0;
      }
    }

    /* Read more */
    matcher->read = fread(matcher->buffer, 1, BUFFER_SIZE, file);
    matcher->index = 0;

    /* Our loop will break if we read 0 bytes */
    if (0 == matcher->read) {
      if (feof(file)) {
        (*err_code) = EOF;
        return (*__line) ? 0 : -1;
      } else {
        (*err_code) = ferror(file);
        return -1;
      }
    }
  } while (1);
}

/**
 * A function started in a pthread which reads all
 * the files in the queue and compiles them into
 * regexes.
 *
 * @param arg            A struct word_matcher
 */
static void *read_regexes(void *arg) {
  struct word_matcher *matcher = (struct word_matcher *)arg;
  int err_code = 0;

  /* We need a place to save the last node in the regex list */
  struct regex_list *last_regex = NULL;
  struct error_list *last_error = NULL;

  /* Loop so long as there are more files to open */
  while (1) {
    /* Check to see if we have been asked to stop */
    pthread_mutex_lock(&matcher->running_mutex);
    if (!matcher->running) {
      pthread_mutex_unlock(&matcher->running_mutex);
      pthread_mutex_unlock(&matcher->regexes_mutex);
      return NULL;
    }
    pthread_mutex_unlock(&matcher->running_mutex);

    /* Get next path */
    const char *current_path;
    matcher->paths = path_queue_pop(matcher->paths, &current_path);
    if (!current_path) {
      pthread_mutex_unlock(&matcher->regexes_mutex);
      return NULL;
    }

    /* Try to open the file */
    FILE *file = fopen(current_path, "r");
    if (!file) {
      last_error = add_error(matcher, last_error, current_path, NULL, errno);
      continue;
    }

    char *line = NULL;
    struct regex_list *new_regex = NULL;
    while (-1 != read_next_regex(matcher, file, &line, &err_code)) {
      /* Check again to see if we have been asked to stop */
      pthread_mutex_lock(&matcher->running_mutex);
      if (!matcher->running) {
        pthread_mutex_unlock(&matcher->running_mutex);
        if (new_regex)
          free(new_regex);
        fclose(file);
        if (line)
          free(line);
        pthread_mutex_unlock(&matcher->regexes_mutex);
        return NULL;
      }
      pthread_mutex_unlock(&matcher->running_mutex);

      /* We only want to call malloc if we need to. When an invalid
         regex string is encountered, we can still reuse that memory
         block. */
      if (NULL == new_regex) {
        new_regex = (struct regex_list *)malloc(sizeof(struct regex_list));

        /* There might be a better way to handle this in the future,
           but for now we will just exit if malloc fails */
        if (!new_regex) {
          last_error =
              add_error(matcher, last_error, current_path, line, errno);
          line = NULL;
          continue;
        }
      }

      /* Compile the regex string, which may or may not be valid */
      err_code = regcomp(&new_regex->regex, line, REG_EXTENDED);
      if (err_code) {
        last_error =
            add_error(matcher, last_error, current_path, line, err_code);
        line = NULL;
        continue;
      }

      /* Compilation worked, so now we attach new_regex to the
         linked list. */
      new_regex->next = NULL;

      if (last_regex) {
        last_regex->next = new_regex;
      } else {
        matcher->regexes = new_regex;
      }
      last_regex = new_regex;

      /* Set new_regex to NULL so that malloc will be called at
         the top of the loop. */
      new_regex = NULL;
    }

    if (EOF != err_code) {
      last_error = add_error(matcher, last_error, current_path, NULL, err_code);
      if (line)
        free(line);
    }

    /* Close file */
    if (fclose(file)) {
      last_error = add_error(matcher, last_error, current_path, NULL, errno);
    }
  }
}

/**
 * Initialize the word_matcher object and start a thread to read
 * from all files in the path queue.
 *
 * @param matcher         Location of a struct word_matcher
 * @param paths           Queue of paths to open
 * @return                0 on success, or the error code from
 *                        either pthread_mutex_init or
 *                        pthread_create.
 */
int word_matcher_init(struct word_matcher *matcher, struct path_queue *paths) {
  /* Check that matcher is not null */
  if (matcher) {
    /* Init simple variables */
    matcher->regexes = NULL;
    matcher->errors = NULL;
    matcher->paths = paths;
    matcher->running = 1;
    matcher->index = 0;
    matcher->read = 0;

    /* Init things that might fail */
    int err_code = pthread_mutex_init(&matcher->running_mutex, NULL);
    if (err_code) {
      return err_code;
    }

    err_code = pthread_mutex_init(&matcher->regexes_mutex, NULL);
    if (err_code) {
      pthread_mutex_destroy(&matcher->running_mutex);
      return err_code;
    }

    pthread_mutex_lock(&matcher->regexes_mutex);
    err_code = pthread_create(&matcher->thread, NULL, read_regexes, matcher);
    if (err_code) {
      pthread_mutex_unlock(&matcher->regexes_mutex);

      pthread_mutex_destroy(&matcher->running_mutex);
      pthread_mutex_destroy(&matcher->regexes_mutex);
    }
    return err_code;
  }

  return EINVAL;
}

/**
 * Destroy a word_matcher object. Signals to the read_regex
 * thread to stop and then frees all allocated data.
 *
 * @param matcher     Object to destroy
 * @returns           0 on success
 */
int word_matcher_destroy(struct word_matcher *matcher) {
  /* Only act if matcher is nonnull */
  if (matcher) {
    /* Check if read_regexes thread is still running. If
       it is, signal for it to stop. */
    pthread_mutex_lock(&matcher->running_mutex);
    if (matcher->running) {
      /* Set flag to start cleanup */
      matcher->running = 0;
      pthread_mutex_unlock(&matcher->running_mutex);
      /* Wait for thread to cleanup */
      pthread_join(matcher->thread, NULL);
    } else {
      /* Thread has already finished */
      pthread_mutex_unlock(&matcher->running_mutex);
    }

    /* Clean up paths */
    path_queue_destroy(matcher->paths);

    /* Clean up errors */
    {
      struct error_list *cur = matcher->errors;
      while (cur) {
        struct error_list *temp = cur;
        cur = cur->next;

        if (temp->line)
          free(temp->line);
        free(temp);
      }
    }

    /* Clean up regexes */
    {
      pthread_mutex_lock(&matcher->regexes_mutex);
      struct regex_list *cur = matcher->regexes;
      while (cur) {
        struct regex_list *temp = cur;
        cur = cur->next;

        regfree(&temp->regex);
        free(temp);
      }
      matcher->regexes = NULL;
      pthread_mutex_unlock(&matcher->regexes_mutex);
    }

    /* Clean up pthread stuff */
    pthread_mutex_destroy(&matcher->regexes_mutex);
    pthread_mutex_destroy(&matcher->running_mutex);
  }

  return 0;
}

/**
 * Check a word agains all the regexes in this matcher. No special flags
 * are given to the regex.
 *
 * @param matcher      Object with all the regexes
 * @param word         String taken from input files
 * @returns            0 if a match, REG_NOMATCH if not
 */
int word_matcher_match(struct word_matcher *matcher, const char *word) {
  /* Only check if both parameters are nonnull */
  if (matcher && word) {
    pthread_mutex_lock(&matcher->regexes_mutex);

    /* Now its time to iterate over the list */
    struct regex_list *cur = matcher->regexes;
    while (cur) {
      int err_code = regexec(&cur->regex, word, 0, 0, 0);

      /* If this was a match or error, return the status
         code */
      if (REG_NOMATCH != err_code) {
        pthread_mutex_unlock(&matcher->regexes_mutex);
        return err_code;
      }

      cur = cur->next;
    }

    pthread_mutex_unlock(&matcher->regexes_mutex);
  }

  return REG_NOMATCH;
}
