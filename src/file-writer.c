#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

#include <file-writer.h>
#include <token-list.h>

static void *write_tokens(void *arg) {
  struct file_writer *writer = (struct file_writer *)arg;

  /* If we were given an output_mutex, then we need to
     wait until the file_reader unlocks it. */
  if (writer->output_mutex) {
    pthread_mutex_lock(writer->output_mutex);
  }

  /* Try to open the output file */
  FILE *file = fopen(writer->path, "w");
  if (!file) {
    writer->status_code = errno;
    if (writer->output_mutex) {
      pthread_mutex_unlock(writer->output_mutex);
    }
    return NULL;
  }

  /* Run loop until we are asked to stop */
  while (1) {
    pthread_mutex_lock(&writer->tokens_mutex);
    /* We only only going to check if we've been asked to
       stop if there are no tokens in the queue. Even a
       destroyed file_writer shoudl finish the tokens that
       have already been pushed*/
    while (!writer->tokens) {
      /* Check if we've been asked to stop */
      pthread_mutex_lock(&writer->running_mutex);
      if (!writer->running) {
        pthread_mutex_unlock(&writer->running_mutex);
        if (fclose(file)) {
          writer->status_code = errno;
        }
        if (writer->output_mutex) {
          pthread_mutex_unlock(writer->output_mutex);
        }
        pthread_mutex_unlock(&writer->tokens_mutex);
        return NULL;
      }
      pthread_mutex_unlock(&writer->running_mutex);

      /* Now we wait */
      pthread_cond_wait(&writer->tokens_cond, &writer->tokens_mutex);
    }

    /* Pop token from queue */
    struct token_list *token = writer->tokens;
    writer->tokens = writer->tokens->next;
    if (!writer->tokens) {
      /* the queue is now empty */
      writer->last_token = NULL;
    }
    pthread_mutex_unlock(&writer->tokens_mutex);

    /* Write to file */
    if (token->word) {
      if (EOF == fputs(token->word, file)) {
        writer->status_code = ferror(file);
        fclose(file);
        pthread_mutex_lock(&writer->running_mutex);
        writer->running = 0;
        pthread_mutex_unlock(&writer->running_mutex);
        if (writer->output_mutex) {
          pthread_mutex_unlock(writer->output_mutex);
        }
        return NULL;
      }

      free(token->word);
    }

    if (token->space) {
      if (EOF == fputs(token->space, file)) {
        writer->status_code = ferror(file);
        fclose(file);
        pthread_mutex_lock(&writer->running_mutex);
        writer->running = 0;
        pthread_mutex_unlock(&writer->running_mutex);
        if (writer->output_mutex) {
          pthread_mutex_unlock(writer->output_mutex);
        }
        return NULL;
      }

      free(token->space);
    }

    token_list_free(token);
  }
}

/**
 * Initialize the file_writer object. Starts a thread which listens for
 * new tokens added to a queue and writes them when they become available.
 * If the output file is also one of the input files, the file_reader needs
 * to be given the output path and a locked mutex. When it finishes with the
 * output file, the mutex will be unlocked and this object will begin writting
 * to it. If the given mutex is null, both objects will assume that there
 * is not conflict.
 *
 * @param writer                A preallocated, uninitialized object
 * @param output_path           Path to the output file
 * @patam output_mutex          A locked mutex or NULL
 * @return                      0 on succes or an error code from pthread
 */
int file_writer_init(struct file_writer *writer, const char *output_path,
                     pthread_mutex_t *output_mutex) {
  /* Validate params */
  if (!writer || !output_path)
    return EINVAL;

  /* Set easy vars */
  writer->running = 1;
  writer->status_code = 0;
  writer->tokens = NULL;
  writer->last_token = NULL;
  writer->path = output_path;
  writer->output_mutex = output_mutex;

  /* Set vars that might fail */
  int err_code = pthread_mutex_init(&writer->running_mutex, NULL);
  if (err_code) {
    return err_code;
  }

  err_code = pthread_mutex_init(&writer->tokens_mutex, NULL);
  if (err_code) {
    pthread_mutex_destroy(&writer->running_mutex);
    return err_code;
  }

  err_code = pthread_cond_init(&writer->tokens_cond, NULL);
  if (err_code) {
    pthread_mutex_destroy(&writer->running_mutex);
    pthread_mutex_destroy(&writer->tokens_mutex);
    return err_code;
  }

  err_code = pthread_create(&writer->thread, NULL, write_tokens, writer);
  if (err_code) {
    pthread_mutex_destroy(&writer->running_mutex);
    pthread_mutex_destroy(&writer->tokens_mutex);
    pthread_cond_destroy(&writer->tokens_cond);
    return err_code;
  }

  return 0;
}

/**
 * Frees memory allocated for a file_writer object. Stops
 * the thread if its running.
 *
 * @param writer         file_writer object
 * @return               0 on success
 */
int file_writer_destroy(struct file_writer *writer) {
  /* Only act if writter is nonnull */
  if (writer) {
    /* Check if thread is running */
    pthread_mutex_lock(&writer->running_mutex);
    if (writer->running) {
      /* Ask thread to stop */
      writer->running = 0;
      pthread_mutex_unlock(&writer->running_mutex);
      pthread_cond_signal(&writer->tokens_cond);
      pthread_join(writer->thread, NULL);
    } else {
      /* Already stopped */
      pthread_mutex_unlock(&writer->running_mutex);
    }

    /* Free any remaining tokens */
    pthread_mutex_lock(&writer->tokens_mutex);
    struct token_list *cur = writer->tokens;
    while (cur) {
      struct token_list *temp = cur;
      cur = temp->next;
      if (temp->word)
        free(temp->word);
      if (temp->space)
        free(temp->space);
      token_list_free(temp);
    }
    pthread_mutex_unlock(&writer->tokens_mutex);

    /* Destroy pthread objects */
    pthread_mutex_destroy(&writer->running_mutex);
    pthread_mutex_destroy(&writer->tokens_mutex);
    pthread_cond_destroy(&writer->tokens_cond);
  }

  return 0;
}

/**
 * Add a token to the queue of items to be written.
 *
 * @param writer     file_writer instance
 * @param token      token object taken from file_reader
 * @return           0 on success
 */
int file_writer_write(struct file_writer *writer, struct token_list *token) {
  /* only act if both params are nonnull */
  if (writer && token) {
    /* Check if thread is still running. If not, an error occured and we
       return the status_code. */
    pthread_mutex_lock(&writer->running_mutex);
    if (!writer->running) {
      /* Something probably went wrong */
      pthread_mutex_unlock(&writer->running_mutex);
      return writer->status_code;
    }
    pthread_mutex_unlock(&writer->running_mutex);

    /* Everythings cool, add to the queue */
    pthread_mutex_lock(&writer->tokens_mutex);
    if (writer->last_token) {
      writer->last_token->next = token;
    } else {
      writer->tokens = token;
    }
    writer->last_token = token;
    writer->last_token->next = NULL;
    /* Signal change */
    pthread_cond_signal(&writer->tokens_cond);
    pthread_mutex_unlock(&writer->tokens_mutex);
  }

  return 0;
}
