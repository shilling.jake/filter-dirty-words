#include <stdio.h>
#include <stdlib.h>

#include <pthread.h>

#include <token-list.h>

/*

NOTE FOR REVIEWERS.

The idea of these functions was to prevent frequent calls to malloc and free
as the tokens are created and destroyed by both the file_reader and file_writer
at the same time. Most of our malloc calls are for strings of unkown length
and can't really be cached because of that, but the consistent size of the token_list
means that we should be able to reuse the blocks to limit the need to keep 
changing the heap.

Right now these are commented out because they were causing an error in the
file_reader tests, which would only come up sometimes. Because this problem is
tricky to reproduce, its going to take some time to debug and for now I'm
just going to have these functions wrap calls to malloc and free.

*/

//static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
//static struct token_list *head = NULL;
//static struct token_list *tail = NULL;

/**
 * Find a memory block big enough for a struct token_list.
 *
 * @return an allocated, but uninitialized struct token_list,
 *         or NULL on error.
 */
struct token_list *token_list_alloc(void) {
  /* Check to see if there is a pre-allocated block */
  /* pthread_mutex_lock(&mutex);
  if (head) {
    struct token_list *ret = head;
    head = head->next;

    if (!head)
      tail = NULL;

    pthread_mutex_unlock(&mutex);
    return ret;
  } else {
    pthread_mutex_unlock(&mutex);
    return (struct token_list *)malloc(sizeof(struct token_list));
    } */
  return (struct token_list *)malloc(sizeof(struct token_list));
}

/**
 * Save an unneeded block to be used later
 */
void token_list_free(struct token_list *node) {
  /* Push node onto queue */
  /*  if (node) {
    pthread_mutex_lock(&mutex);
    if (tail) {
      tail->next = node;
      tail = node;
    } else {
      head = node;
      tail = node;
    }
    pthread_mutex_unlock(&mutex);
    } */
  if (node)
    free(node);
}

/**
 * Pass all nodes to free
 */
void token_list_free_all(void) {
  /*  pthread_mutex_lock(&mutex);
  while (head) {
    struct token_list *next = head->next;
    free(head);
    head = next;
  }
  tail = NULL;
  pthread_mutex_unlock(&mutex); */
  return;
}
