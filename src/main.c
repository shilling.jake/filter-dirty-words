#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <getopt.h>

#include <pthread.h>

#include <config.h>
#include <file-reader.h>
#include <file-writer.h>
#include <path-queue.h>
#include <token-list.h>
#include <word-matcher.h>

/* Descriptions of long options accepted by GNU's getopt_long */
static struct option long_options[] = {{"words", required_argument, 0, 'w'},
                                       {"input", required_argument, 0, 'i'},
                                       {"output", required_argument, 0, 'o'},
                                       {"help", no_argument, 0, 'h'},
                                       {0, 0, 0, 0}};

/* Help message to show to the user */
static const char *usage =
    "Usage: " PACKAGE " [OPTIONS]\n"
    "Filter dirty words from a file.\n"
    "Example: " PACKAGE " -w dirty-words.txt -i input.txt -o output.txt\n"
    "\n"
    "Required options:\n"
    "  -w, --words=FILE    file containing regular expressions to identify\n"
    "                      a \'dirty word\'. Patterns must be separated by\n"
    "                      new lines\n"
    "  -i, --input=FILE    file to read input words from\n"
    "  -o, --output=FILE   file to output to\n"
    "\n"
    "Report bugs to <" PACKAGE_BUGREPORT ">";

static struct word_matcher matcher;
static struct file_reader reader;
static struct file_writer writer;

static void cleanup(void) {
  for (struct error_list *cur = matcher.errors; cur; cur = cur->next) {
    if (cur->line) {
      printf("Error parsing \'%s\' in file \'%s\': %s\n", cur->line, cur->path,
             strerror(cur->err_code));
    } else {
      printf("Error file \'%s\': %s\n", cur->path, strerror(cur->err_code));
    }
  }

  word_matcher_destroy(&matcher);
  file_reader_destroy(&reader);
  file_writer_destroy(&writer);
  token_list_free_all();
}

int main(int argc, char *argv[]) {

  /* Set up queues to support multiple regex/input files */
  struct path_queue *words_paths = NULL;
  struct path_queue *input_paths = NULL;

  /* We will only write to one output file */
  const char *output_path = NULL;

  /*
   * PARSE COMMAND LINE ARGUMENTS
   */

  while (1) {
    /* getopt_long needs an int to place the argument index into */
    int option_index = 0;
    /* Get next option */
    int c = getopt_long(argc, argv, "?hw:i:o:", long_options, &option_index);

    /* getopt_long returns -1 when it's finished */
    if (-1 == c)
      break;

    switch (c) {
    case 'w':
      /* Add a regex file to the queue */
      words_paths = path_queue_push(words_paths, optarg);
      break;
    case 'i':
      /* Add an input file to the queue */
      input_paths = path_queue_push(input_paths, optarg);
      break;
    case 'o':
      /* Set the output path if not already */
      if (output_path) {
        fprintf(stderr, "You can only specify one output file.\n");
        exit(EXIT_FAILURE);
      } else {
        output_path = optarg;
      }
      break;
    case 'h':
    case '?':
      /* The user needs help. Print a message and exit */
      puts(usage);
      exit(EXIT_SUCCESS);
      break;
    default:
      /* We should never come here. Something went wrong if we do. */
      fprintf(stderr, "Something when wrong while parsing options.");
      exit(EXIT_FAILURE);
      break;
    }
  }

  /*
   * VALIDATE INPUT
   */

  if (!output_path) {
    fprintf(stderr, "You must specify an output file\n");
    if (words_paths)
      path_queue_destroy(words_paths);
    if (input_paths)
      path_queue_destroy(input_paths);
    exit(EXIT_FAILURE);
  }

  if (!words_paths) {
    fprintf(stderr, "You must specify at least one dirty words file.\n");
    if (words_paths)
      path_queue_destroy(words_paths);
    if (input_paths)
      path_queue_destroy(input_paths);
    exit(EXIT_FAILURE);
  }

  if (!input_paths) {
    fprintf(stderr, "You must specify at least one input file.\n");
    if (words_paths)
      path_queue_destroy(words_paths);
    if (input_paths)
      path_queue_destroy(input_paths);
    exit(EXIT_FAILURE);
  }

  int output_is_input = 0;
  if (path_queue_contains(input_paths, output_path)) {
    output_is_input = 1;
  }

  /*
   * START READING FROM FILES
   */

  /* Start parsing regexes */
  int err_code = word_matcher_init(&matcher, words_paths);
  if (err_code) {
    fprintf(stderr, "Error initializing matcher: %s\n", strerror(err_code));
    if (words_paths)
      path_queue_destroy(words_paths);
    if (input_paths)
      path_queue_destroy(input_paths);
    exit(EXIT_FAILURE);
  }

  /* Start reading input */
  pthread_mutex_t output_mutex;

  if (output_is_input) {
    err_code = pthread_mutex_init(&output_mutex, NULL);
    if (err_code) {
      fprintf(stderr, "Error initializing output mutex: %s\n",
              strerror(err_code));
      word_matcher_destroy(&matcher);
      if (input_paths)
        path_queue_destroy(input_paths);
      exit(EXIT_FAILURE);
    }

    pthread_mutex_lock(&output_mutex);

    err_code =
        file_reader_init(&reader, input_paths, output_path, &output_mutex);
    if (err_code) {
      fprintf(stderr, "Error initializing reader: %s\n", strerror(err_code));
      word_matcher_destroy(&matcher);
      pthread_mutex_destroy(&output_mutex);
      if (input_paths)
        path_queue_destroy(input_paths);
      exit(EXIT_FAILURE);
    }
  } else {
    err_code = file_reader_init(&reader, input_paths, NULL, NULL);
    if (err_code) {
      fprintf(stderr, "Error initializing reader: %s\n", strerror(err_code));
      word_matcher_destroy(&matcher);
      pthread_mutex_destroy(&output_mutex);
      if (input_paths)
        path_queue_destroy(input_paths);
      exit(EXIT_FAILURE);
    }
  }

  /* Start output */
  if (output_is_input) {
    err_code = file_writer_init(&writer, output_path, &output_mutex);
    if (err_code) {
      fprintf(stderr, "Error initializing reader: %s\n", strerror(err_code));
      word_matcher_destroy(&matcher);
      pthread_mutex_destroy(&output_mutex);
      file_reader_destroy(&reader);
      token_list_free_all();
      exit(EXIT_FAILURE);
    }
  } else {
    err_code = file_writer_init(&writer, output_path, NULL);
    if (err_code) {
      fprintf(stderr, "Error initializing reader: %s\n", strerror(err_code));
      word_matcher_destroy(&matcher);
      file_reader_destroy(&reader);
      token_list_free_all();
      exit(EXIT_FAILURE);
    }
  }

  /* Everything is started, so now clean up is the same no matter
     when we exit */
  atexit(cleanup);

  /* Do the filtering */
  struct token_list *token;
  while (1) {
    if (file_reader_read(&reader, &token)) {
      if (EOF != reader.status_code) {
        /* An error occured while reading */
        fprintf(stderr, "Error while reading %s: %s\n", reader.current_path,
                strerror(reader.status_code));
        if (output_is_input)
          pthread_mutex_destroy(&output_mutex);
        exit(EXIT_FAILURE);
      } else {
        /* Finished normally */
        if (output_is_input)
          pthread_mutex_destroy(&output_mutex);
        exit(EXIT_SUCCESS);
      }
    }

    /* Try to match */
    if (word_matcher_match(&matcher, token->word)) {
      /* No match */
      if (file_writer_write(&writer, token)) {
        fprintf(stderr, "Error while writting to %s: %s\n", output_path,
                strerror(writer.status_code));
        if (output_is_input)
          pthread_mutex_destroy(&output_mutex);
        exit(EXIT_FAILURE);
      }
    }
  }
}
