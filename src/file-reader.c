#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <pthread.h>

#include <file-reader.h>
#include <path-queue.h>

/* Copy over a chunk of either continuous whitespace or nonwhitespace characters
 * from buffer into dest. Reallocates dest if needed.
 */
static inline int copy_chunk(size_t *__index, char *buffer, size_t buffer_size,
                             char **__dest, size_t *__dest_size) {
  size_t index = (*__index);
  char *dest = (*__dest);
  size_t dest_size = (*__dest_size);
  int is_space = isspace(buffer[index]);

  size_t start = index;
  while ((index < buffer_size) && (is_space == isspace(buffer[index]))) {
    index++;
  }

  size_t chunk_size = index - start;
  dest = realloc(dest, dest_size + chunk_size + 1);
  if (!dest) {
    (*__dest) = NULL;
    return -1;
  }

  memcpy(dest + dest_size, buffer + start, chunk_size);
  dest_size += chunk_size;
  dest[dest_size] = '\0';

  (*__index) = index;
  (*__dest) = dest;
  (*__dest_size) = dest_size;

  return 0;
}

/* Creates a new token and addes it to the queue in reader */
static inline int add_new_token(struct file_reader *reader, char **word,
                                size_t *word_size, char **space,
                                size_t *space_size) {
  struct token_list *node = token_list_alloc();
  if (!node) {
    return -1;
  }

  node->word = (*word);
  node->space = (*space);
  node->next = NULL;

  (*word) = NULL;
  (*word_size) = 0;
  (*space) = NULL;
  (*space_size) = 0;

  pthread_mutex_lock(&reader->tokens_mutex);
  if (reader->last_token) {
    reader->last_token->next = node;
  } else {
    reader->tokens = node;
  }
  reader->last_token = node;
  pthread_cond_signal(&reader->tokens_cond);
  pthread_mutex_unlock(&reader->tokens_mutex);

  return 0;
}

static void *read_files(void *arg) {
  struct file_reader *reader = (struct file_reader *)arg;

  /* Setup places to save tokens to */
  char *word = NULL;
  size_t word_size = 0;
  char *space = NULL;
  size_t space_size = 0;

  FILE *file = NULL;

  /* Get the first path */
  reader->paths = path_queue_pop(reader->paths, &reader->current_path);
  while (reader->current_path) {
    /* Check to see if we've been asked to stop */
    pthread_mutex_lock(&reader->running_mutex);
    if (!reader->running) {
      pthread_mutex_unlock(&reader->running_mutex);
      return NULL;
    }
    pthread_mutex_unlock(&reader->running_mutex);

    /* Try to open the file for reading */
    file = fopen(reader->current_path, "r");
    if (!file) {
      /* We don't want to just skip over a bad file. That might
         mess up the ordering of the tokens in the output file.
         Instead just stop. */
      reader->status_code = errno;
      goto exit;
    }

    /* Start reading the file. We want to find words which are
       separated by whitespace. */
    size_t read;
    do {
      /* Check to see if we've been asked to stop */
      pthread_mutex_lock(&reader->running_mutex);
      if (!reader->running) {
        pthread_mutex_unlock(&reader->running_mutex);
        return NULL;
      }
      pthread_mutex_unlock(&reader->running_mutex);

      read = fread(reader->buffer, 1, BUFFER_SIZE, file);

      /* We are now at the beginning of a buffer, but not necessarily
         of the file. Parts of a word or space may have been read from
         the previous buffer. We need to figure out what we are looking
         for. */
      size_t index = 0;
      while (index < read) {
        /* Check to see if we are at the end of a token. We know that
           if we have already read some whitespace and the current
           character is the start of a word. */
        if (space && !isspace(reader->buffer[index])) {
          /* If word is null, then put blank text before the space */
          if (!word) {
            word = (char *)malloc(1);
            if (!word) {
              reader->status_code = errno;
              goto exit;
            }
            word[0] = '\0';
          }

          if (add_new_token(reader, &word, &word_size, &space, &space_size)) {
            reader->status_code = errno;
            goto exit;
          }
        }

        if (isspace(reader->buffer[index])) {
          if (copy_chunk(&index, reader->buffer, read, &space, &space_size)) {
            reader->status_code = errno;
            goto exit;
          }
        } else {
          if (copy_chunk(&index, reader->buffer, read, &word, &word_size)) {
            reader->status_code = errno;
            goto exit;
          }
        }
      }

    } while (read == BUFFER_SIZE);

    /* Check if the loop broke because of an error or EOF */
    if (feof(file)) {
      /* We are at the end of a file. If there is a word add
         it to the queue. If this word is not followed by
         whitespace add a newline to represent the EOF */
      if (word) {
        if (!space) {
          space = (char *)malloc(2);
          if (!space) {
            reader->status_code = errno;
            goto exit;
          }
          space[0] = '\n';
          space[1] = '\0';
        }

        if (add_new_token(reader, &word, &word_size, &space, &space_size)) {
          reader->status_code = errno;
          goto exit;
        }
      }
    } else {
      /* An error occured reading the file */
      reader->status_code = ferror(file);
      goto exit;
    }

    /* We finished with that file */
    fclose(file);
    file = NULL;

    /* If we were given an output_path, then the output file is also
       an input file. Check if we have just finished with that output
       file */
    if (reader->output_path) {
      if (!strcmp(reader->current_path, reader->output_path)) {
        pthread_mutex_unlock(reader->output_mutex);
      }
    }

    reader->paths = path_queue_pop(reader->paths, &reader->current_path);
  }

  /* We have read all the files */
  reader->status_code = EOF;

exit:
  /* Close unfree'd resources */
  if (file)
    fclose(file);
  if (space)
    free(space);
  if (word)
    free(word);

  /* All finished! */
  pthread_mutex_lock(&reader->running_mutex);
  reader->running = 0;
  pthread_mutex_unlock(&reader->running_mutex);

  /* Signal to any waiting threads to wake up */
  pthread_cond_signal(&reader->tokens_cond);

  return NULL;
}

/**
 * Initialize file_reader object and start a thread to read all input
 * files.
 *
 * @param reader       An uninitialized struct file_reader.
 * @param paths        Queue of paths to input files
 * @param output_path  Path to output file, or NULL
 * @pathm output_mutex Locked mutex, or NULL
 * @return             0 on succes, or a pthread err code
 */
int file_reader_init(struct file_reader *reader, struct path_queue *paths,
                     const char *output_path, pthread_mutex_t *output_mutex) {
  /* Start with the vars that cannot fail */
  reader->paths = paths;
  reader->tokens = NULL;
  reader->last_token = NULL;
  reader->current_path = NULL;
  reader->status_code = 0;
  reader->running = 1;
  reader->output_path = output_path;
  reader->output_mutex = output_mutex;

  /* Now do the pthread stuff. Cleanup after any failure */
  int err_code = pthread_mutex_init(&reader->running_mutex, NULL);
  if (err_code) {
    return err_code;
  }

  err_code = pthread_mutex_init(&reader->tokens_mutex, NULL);
  if (err_code) {
    pthread_mutex_destroy(&reader->running_mutex);
    return err_code;
  }

  err_code = pthread_cond_init(&reader->tokens_cond, NULL);
  if (err_code) {
    pthread_mutex_destroy(&reader->running_mutex);
    pthread_mutex_destroy(&reader->tokens_mutex);
    return err_code;
  }

  err_code = pthread_create(&reader->thread, NULL, read_files, reader);
  if (err_code) {
    pthread_mutex_destroy(&reader->running_mutex);
    pthread_mutex_destroy(&reader->tokens_mutex);
    pthread_cond_destroy(&reader->tokens_cond);

    return err_code;
  }

  return 0;
}

/**
 * Free all data allocated by this reader object. Stops
 * the read_files thread if its still running.
 *
 * @param reader        Object to fre
 * @return              0 on success
 */
int file_reader_destroy(struct file_reader *reader) {
  /* Only act if reader is nonnull */
  if (reader) {
    /* Check if the thread is running */
    pthread_mutex_lock(&reader->running_mutex);
    if (reader->running) {
      /* Is running, ask it to stop */
      reader->running = 0;
      pthread_mutex_unlock(&reader->running_mutex);
      pthread_join(reader->thread, NULL);
    } else {
      /* Is not running */
      pthread_mutex_unlock(&reader->running_mutex);
    }

    /* Free paths */
    path_queue_destroy(reader->paths);

    /* Free tokens list */
    pthread_mutex_lock(&reader->tokens_mutex);
    struct token_list *cur = reader->tokens;
    while (cur) {
      struct token_list *temp = cur;
      cur = temp->next;

      if (temp->word)
        free(temp->word);
      if (temp->space)
        free(temp->space);
      token_list_free(temp);
    }
    reader->tokens = NULL;
    pthread_mutex_unlock(&reader->tokens_mutex);

    /* Destroy pthread objects */
    pthread_mutex_destroy(&reader->running_mutex);
    pthread_mutex_destroy(&reader->tokens_mutex);
    pthread_cond_destroy(&reader->tokens_cond);
  }

  return 0;
}

/**
 * Get the next token_list from files.
 *
 * @param tokens        Address to put next token_list into. (*tokens)
 *                      will be set to NULL if there are not more tokens.
 * @return 0 on success, EOF went there's nothing left
 */
int file_reader_read(struct file_reader *reader, struct token_list **token) {
  /* Only act if all params are nonnull */
  if (reader && token) {
    pthread_mutex_lock(&reader->tokens_mutex);

    /* Try to get the head of the queue */
    while (!reader->tokens) {
      /* Check if we are still running */
      pthread_mutex_lock(&reader->running_mutex);
      if (reader->running) {
        pthread_mutex_unlock(&reader->running_mutex);
        /* Just need to wait */
        pthread_cond_wait(&reader->tokens_cond, &reader->tokens_mutex);
      } else {
        /* Not running, nothing to wait for */
        pthread_mutex_unlock(&reader->running_mutex);
        pthread_mutex_unlock(&reader->tokens_mutex);
        /* No more items */
        (*token) = NULL;
        return reader->status_code;
      }
    }

    /* Set return token */
    (*token) = reader->tokens;
    if (reader->tokens == reader->last_token) {
      reader->tokens = NULL;
      reader->last_token = NULL;
    } else {
      reader->tokens = reader->tokens->next;
    }

    pthread_mutex_unlock(&reader->tokens_mutex);
  }

  return 0;
}
