#ifndef WORD_MATCHER_H
#define WORD_MATCHER_H

#include <path-queue.h>
#include <regex.h>

#ifndef BUFFER_SIZE
#define BUFFER_SIZE (128)
#endif

struct regex_list {
  regex_t regex;
  struct regex_list *next;
};

struct error_list {
  const char *path;
  char *line;
  int err_code;
  struct error_list *next;
};

struct word_matcher {
  pthread_t thread;

  int running;
  pthread_mutex_t running_mutex;

  struct error_list *errors;
  struct regex_list *regexes;
  pthread_mutex_t regexes_mutex;

  struct path_queue *paths;

  char buffer[BUFFER_SIZE];
  size_t index;
  size_t read;
};

int word_matcher_init(struct word_matcher *, struct path_queue *);
int word_matcher_destroy(struct word_matcher *);
int word_matcher_match(struct word_matcher *, const char *);

#endif /* WORD_MATCHER_H */
