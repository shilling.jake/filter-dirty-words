#ifndef TOKEN_LIST_H
#define TOKEN_LIST_H

struct token_list {
  char *word;
  char *space;
  struct token_list *next;
};

struct token_list *token_list_alloc(void);
void token_list_free(struct token_list *);
void token_list_free_all(void);

#endif /* TOKEN_LIST_H */
