#ifndef FILE_READER_H
#define FILE_READER_H

#include <pthread.h>

#include <path-queue.h>
#include <token-list.h>

#ifndef BUFFER_SIZE
#define BUFFER_SIZE (128)
#endif

struct file_reader {
  pthread_t thread;
  int running;
  pthread_mutex_t running_mutex;

  struct path_queue *paths;
  int status_code;
  const char *current_path;

  const char *output_path;
  pthread_mutex_t *output_mutex;

  struct token_list *tokens;
  struct token_list *last_token;
  pthread_mutex_t tokens_mutex;
  pthread_cond_t tokens_cond;

  char buffer[BUFFER_SIZE];
};

int file_reader_init(struct file_reader *, struct path_queue *, const char *,
                     pthread_mutex_t *);
int file_reader_destroy(struct file_reader *);

int file_reader_read(struct file_reader *, struct token_list **);

#endif /* FILE_READER_H */
