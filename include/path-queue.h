
#ifndef PATH_QUEUE_H
#define PATH_QUEUE_H

struct path_queue {
  const char *path;
  struct path_queue *next;
  struct path_queue *prev;
};

struct path_queue *path_queue_push(struct path_queue *, const char *);
struct path_queue *path_queue_pop(struct path_queue *, const char **);

void path_queue_destroy(struct path_queue *);

int path_queue_contains(struct path_queue *, const char *);

#endif /* PATH_QUEUE_H */
