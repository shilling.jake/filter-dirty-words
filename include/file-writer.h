#ifndef FILE_WRITER_H
#define FILE_WRITER_H

#include <pthread.h>

#include <token-list.h>

struct file_writer {
  pthread_t thread;
  int running;
  int status_code;
  pthread_mutex_t running_mutex;

  struct token_list *tokens;
  struct token_list *last_token;
  pthread_mutex_t tokens_mutex;
  pthread_cond_t tokens_cond;

  const char *path;
  pthread_mutex_t *output_mutex;
};

int file_writer_init(struct file_writer *, const char *, pthread_mutex_t *);
int file_writer_destroy(struct file_writer *);

int file_writer_write(struct file_writer *, struct token_list *);

#endif /* FILE_WRITER_H */
