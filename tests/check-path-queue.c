#include <stdlib.h>

#include <check.h>

#include <path-queue.h>

static const char *first = "first";
static const char *second = "second";
static const char *third = "third";
static const char *fourth = "fourth";

START_TEST(test_path_queue_push) {
  struct path_queue *queue = NULL;

  /* push first item into the queue */
  queue = path_queue_push(queue, first);
  ck_assert_ptr_nonnull(queue);
  ck_assert_ptr_eq(queue->path, first);
  ck_assert_ptr_null(queue->next);

  /* push second item into the queue */
  queue = path_queue_push(queue, second);
  ck_assert_ptr_nonnull(queue);
  ck_assert_ptr_eq(queue->path, first);
  ck_assert_ptr_nonnull(queue->next);
  ck_assert_ptr_eq(queue->next->path, second);

  /* push third item into the queue */
  queue = path_queue_push(queue, third);
  ck_assert_ptr_nonnull(queue);
  ck_assert_ptr_eq(queue->path, first);
  ck_assert_ptr_nonnull(queue->next);
  ck_assert_ptr_eq(queue->next->path, second);
  ck_assert_ptr_nonnull(queue->next->next);
  ck_assert_ptr_eq(queue->next->next->path, third);

  /* push fourth item into the queue */
  queue = path_queue_push(queue, fourth);
  ck_assert_ptr_nonnull(queue);
  ck_assert_ptr_eq(queue->path, first);
  ck_assert_ptr_nonnull(queue->next);
  ck_assert_ptr_eq(queue->next->path, second);
  ck_assert_ptr_nonnull(queue->next->next);
  ck_assert_ptr_eq(queue->next->next->path, third);
  ck_assert_ptr_nonnull(queue->next->next->next);
  ck_assert_ptr_eq(queue->next->next->next->path, fourth);

  /* cleanup */
  path_queue_destroy(queue);
}
END_TEST

START_TEST(test_path_queue_pop) {
  /* set up a queue */
  struct path_queue *queue = NULL;

  queue = path_queue_push(queue, first);
  queue = path_queue_push(queue, second);
  queue = path_queue_push(queue, third);
  queue = path_queue_push(queue, fourth);

  const char *result = NULL;

  /* Start popping */
  queue = path_queue_pop(queue, &result);
  ck_assert_ptr_eq(result, first);

  queue = path_queue_pop(queue, &result);
  ck_assert_ptr_eq(result, second);

  queue = path_queue_pop(queue, &result);
  ck_assert_ptr_eq(result, third);

  queue = path_queue_pop(queue, &result);
  ck_assert_ptr_eq(result, fourth);

  queue = path_queue_pop(queue, &result);
  ck_assert_ptr_null(result);
  ck_assert_ptr_null(queue);

  /* clean up */
  path_queue_destroy(queue);
}
END_TEST

int main(void) {
  Suite *suite = suite_create("Path Queue");
  TCase *tc_core = tcase_create("Core");

  tcase_add_test(tc_core, test_path_queue_push);
  tcase_add_test(tc_core, test_path_queue_pop);

  suite_add_tcase(suite, tc_core);

  SRunner *runner = srunner_create(suite);
  srunner_set_fork_status(runner, CK_NOFORK);
  srunner_run_all(runner, CK_VERBOSE);
  int number_failed = srunner_ntests_failed(runner);
  srunner_free(runner);

  return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
