#include <stdio.h>
#include <stdlib.h>

#include <check.h>

#include <file-writer.h>
#include <token-list.h>

START_TEST(test_file_writer_init) {
  static const char *output_path = "file-writer-test-1";
  struct file_writer writer;

  ck_assert_int_eq(0, file_writer_init(&writer, output_path, NULL));
  ck_assert_int_eq(0, file_writer_destroy(&writer));

  remove(output_path);
}
END_TEST

START_TEST(test_file_writer_write) {
  static const char *output_path = "file-writer-test-2";
  struct file_writer writer;

  ck_assert_int_eq(0, file_writer_init(&writer, output_path, NULL));

  /* Write a couple of tokens */
  struct token_list *token = token_list_alloc();
  token->word = strdup("one");
  token->space = strdup(" \n");

  ck_assert_int_eq(0, file_writer_write(&writer, token));

  token = token_list_alloc();
  token->word = strdup("two");
  token->space = strdup("\t");

  ck_assert_int_eq(0, file_writer_write(&writer, token));

  /* Destroy the writer */
  ck_assert_int_eq(0, file_writer_destroy(&writer));

  /* Examine the file */
  FILE *file = fopen(output_path, "r");
  ck_assert_ptr_nonnull(file);

  /* file should contain "one \ntwo\t" (9 chars) */
  char buffer[15]; // bigger than needed so we can check for extra chars
  size_t read = fread(buffer, 1, 15, file);
  fclose(file);

  ck_assert_int_eq(9, read);
  /* Add a terminating character so we can use strcmp */
  buffer[9] = '\0';
  ck_assert_str_eq("one \ntwo\t", buffer);

  remove(output_path);
}
END_TEST

int main(void) {
  Suite *suite = suite_create("File Writer");
  TCase *tc_core = tcase_create("Core");

  tcase_add_test(tc_core, test_file_writer_init);
  tcase_add_test(tc_core, test_file_writer_write);

  suite_add_tcase(suite, tc_core);

  SRunner *runner = srunner_create(suite);
  srunner_set_fork_status(runner, CK_NOFORK);
  srunner_run_all(runner, CK_VERBOSE);
  int number_failed = srunner_ntests_failed(runner);
  srunner_free(runner);

  return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
