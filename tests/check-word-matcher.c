#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <pthread.h>

#include <check.h>

#include <word-matcher.h>

struct test_file {
  const char *path;
  const char *text;
};

static struct test_file test_files[] =
  {
     {"simple_word", "apple\nbanana"},
     {"no_numbers", "[0-9]+"},
     {"no_ats", ".at"},
     {0, 0}
  };

START_TEST(test_word_matcher_init) {
  struct path_queue *paths = NULL;

  for (struct test_file *cur = test_files; cur->path && cur->text; cur++) {
    paths = path_queue_push(paths, cur->path);
  }

  /* Create object and join thread */
  struct word_matcher matcher;
  ck_assert_int_eq(0, word_matcher_init(&matcher, paths));
  ck_assert_int_eq(0, pthread_join(matcher.thread, NULL));
  ck_assert_int_eq(0, word_matcher_destroy(&matcher));
}
END_TEST

START_TEST(test_word_matcher_match)
{
  struct path_queue *paths = NULL;

  for (struct test_file *cur = test_files; cur->path && cur->text; cur++) {
    paths = path_queue_push(paths, cur->path);
  }

  /* Create object and join thread */
  struct word_matcher matcher;
  ck_assert_int_eq(0, word_matcher_init(&matcher, paths));
 
  /* Check some matches */
  ck_assert_int_eq(0, word_matcher_match(&matcher, "apple"));
  ck_assert_int_eq(0, word_matcher_match(&matcher, "banana"));
  ck_assert_int_eq(0, word_matcher_match(&matcher, "bat"));
  ck_assert_int_eq(0, word_matcher_match(&matcher, "cat"));
  ck_assert_int_eq(0, word_matcher_match(&matcher, "10"));

  /* Check some non-matches */
  ck_assert_int_eq(REG_NOMATCH, word_matcher_match(&matcher, "table"));
  ck_assert_int_eq(REG_NOMATCH, word_matcher_match(&matcher, "chair"));

  ck_assert_int_eq(0, word_matcher_destroy(&matcher));
}
END_TEST

int main(void) {
  /* Write some files to test */
  for (struct test_file *cur = test_files; cur->path && cur->text; cur++) {
    FILE *file = fopen(cur->path, "w");
    if (file) {
      if (EOF == fputs(cur->text, file)) {
        perror("Couldn't write file");
        return EXIT_FAILURE;
      }
      if (fclose(file)) {
        perror("Counld't close file");
      }
    } else {
      perror("Couldn't open file");
      return EXIT_FAILURE;
    }
  }

  Suite *suite = suite_create("Word Matcher");
  TCase *tc_core = tcase_create("Core");

  tcase_add_test(tc_core, test_word_matcher_init);
  tcase_add_test(tc_core, test_word_matcher_match);

  suite_add_tcase(suite, tc_core);

  SRunner *runner = srunner_create(suite);
  srunner_set_fork_status(runner, CK_NOFORK);
  srunner_run_all(runner, CK_VERBOSE);
  int number_failed = srunner_ntests_failed(runner);
  srunner_free(runner);

  /* Delete files we created for the tests */
  for (struct test_file *cur = test_files; cur->path; cur++) {
    if (remove(cur->path)) {
      fprintf(stderr, "Error removing temporary file %s: %s\n", cur->path,
              strerror(errno));
    }
  }

  return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
