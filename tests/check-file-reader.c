#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <check.h>

#include <file-reader.h>

#define TOKEN_ONE "one"
#define TOKEN_ONE_SPACE "  "
#define TOKEN_TWO "two"
#define TOKEN_TWO_SPACE "\n"

#define TOKEN_THREE ""
#define TOKEN_THREE_SPACE "\t\n"
#define TOKEN_FOUR "four"
#define TOKEN_FOUR_SPACE "\n"

struct test_file {
  const char *path;
  const char *text;
};

static struct test_file test_files[] = {
    {"check-reader-test-file-1", TOKEN_ONE TOKEN_ONE_SPACE TOKEN_TWO},
    {"check-reader-test-file-2", TOKEN_THREE TOKEN_THREE_SPACE TOKEN_FOUR},
    {0, 0}};

START_TEST(test_file_reader_init) {
  /* Collect paths into queue */
  struct path_queue *paths = NULL;
  for (struct test_file *cur = test_files; cur->path && cur->text; cur++) {
    paths = path_queue_push(paths, cur->path);
  }

  /* Build and destroy reader */
  struct file_reader reader;
  ck_assert_int_eq(0, file_reader_init(&reader, paths, NULL, NULL));
  ck_assert_int_eq(0, file_reader_destroy(&reader));
}
END_TEST

START_TEST(test_file_reader_read) {
  /* Collect paths into queue */
  struct path_queue *paths = NULL;
  for (struct test_file *cur = test_files; cur->path && cur->text; cur++) {
    paths = path_queue_push(paths, cur->path);
  }

  /* Build Reader */
  struct file_reader reader;
  ck_assert_int_eq(0, file_reader_init(&reader, paths, NULL, NULL));

  /* Read tokens one at a time, there should be four. */
  struct token_list *token = NULL;

  ck_assert_int_eq(0, file_reader_read(&reader, &token));
  ck_assert_ptr_nonnull(token);
  ck_assert_str_eq(TOKEN_ONE, token->word);
  ck_assert_str_eq(TOKEN_ONE_SPACE, token->space);
  free(token->word);
  free(token->space);
  token_list_free(token);

  ck_assert_int_eq(0, file_reader_read(&reader, &token));
  ck_assert_ptr_nonnull(token);
  if (strcmp(token->word, TOKEN_TWO)) {
    puts("About to fail!");
  }
  ck_assert_str_eq(TOKEN_TWO, token->word);
  ck_assert_str_eq(TOKEN_TWO_SPACE, token->space);
  free(token->word);
  free(token->space);
  token_list_free(token);

  ck_assert_int_eq(0, file_reader_read(&reader, &token));
  ck_assert_ptr_nonnull(token);
  ck_assert_str_eq(TOKEN_THREE, token->word);
  ck_assert_str_eq(TOKEN_THREE_SPACE, token->space);
  free(token->word);
  free(token->space);
  token_list_free(token);

  ck_assert_int_eq(0, file_reader_read(&reader, &token));
  ck_assert_ptr_nonnull(token);
  if (strcmp(token->word, TOKEN_FOUR)) {
    puts("About to fail!");
  }
  ck_assert_str_eq(TOKEN_FOUR, token->word);
  ck_assert_str_eq(TOKEN_FOUR_SPACE, token->space);
  free(token->word);
  free(token->space);
  token_list_free(token);

  ck_assert_int_eq(EOF, file_reader_read(&reader, &token));
  ck_assert_ptr_null(token);

  /* Destroy reader */
  ck_assert_int_eq(0, file_reader_destroy(&reader));
}
END_TEST

int main(void) {
  /* Write some files to test */
  for (struct test_file *cur = test_files; cur->path && cur->text; cur++) {
    FILE *file = fopen(cur->path, "w");
    if (file) {
      if (EOF == fputs(cur->text, file)) {
        perror("Couldn't write file");
        return EXIT_FAILURE;
      }
      if (fclose(file)) {
        perror("Counld't close file");
      }
    } else {
      perror("Couldn't open file");
      return EXIT_FAILURE;
    }
  }

  Suite *suite = suite_create("File Reader");
  TCase *tc_core = tcase_create("Core");

  tcase_add_test(tc_core, test_file_reader_init);
  tcase_add_test(tc_core, test_file_reader_read);

  suite_add_tcase(suite, tc_core);

  SRunner *runner = srunner_create(suite);
  srunner_set_fork_status(runner, CK_NOFORK);
  srunner_run_all(runner, CK_VERBOSE);
  int number_failed = srunner_ntests_failed(runner);
  srunner_free(runner);

  /* Delete files we created for the tests */
  for (struct test_file *cur = test_files; cur->path; cur++) {
    if (remove(cur->path)) {
      fprintf(stderr, "Error removing temporary file %s: %s\n", cur->path,
              strerror(errno));
    }
  }

  return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
