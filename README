# Filter Dirty Words

A program which removes dirty words from a file.

# To build

This program requires a few POSIX functions including pthreads.

run `./build.sh` or

```
./configure
make
```

Either command will compile produce a program at `./src/filter-dirty-words`

# To run

Exactly one output file must be specified with he -o/--output flag, at least
one dirty words file with -w/--words, and at least one input file with
-i/--input. If an error is encountered while reading inputs or writting output
execution will stop. If an error is encountered reading from a dirty words
file execution will continue with that file skipped.

View usage instructions with:

```
./src/filter-dirty-words --help
```

Which should print

```
Usage: filter-dirty-words [OPTIONS]
Filter dirty words from a file.
Example: filter-dirty-words -w dirty-words.txt -i input.txt -o output.txt

Required options:
  -w, --words=FILE    file containing regular expressions to identify
                      a 'dirty word'. Patterns must be separated by
                      new lines
  -i, --input=FILE    file to read input words from
  -o, --output=FILE   file to output to

Report bugs to <shilling.jake@gmail.com>
```